'''
In this file I will put many of the basic cookbook recipes,
small pieces of code that I use in my everyday life.
This code will grow as we stumble into problems that call
for me to show more and more examples.

You can change the code here too, but let us try not to make
a mess of it.
'''

import numpy as np # for basic mathematical operations with arrays
import pandas as pd # for table-data manipulation
import matplotlib.pyplot as plt # for producing some plots



