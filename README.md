## Modelab Learning Community (MLC) ##
# Playground#

This is a markdown README document for the MLC playground. Feel free to edit everything in this repository without fear of breaking things. This is what it is for.

There are several things you should learn about repositories:

* How to write markdown documents as this one. You can find a tutorial in [here](https://bitbucket.org/tutorials/markdowndemo).
* Learn to use git
* Learn to use git repositories such as github or bitbucket. We are using bitbucket because it allows us to have private repositories. You can learn more in [here](https://www.atlassian.com/git/tutorials/learn-git-with-bitbucket-cloud).

You have probably already read some things about git. But there is no better way of learning that messing things up. This is the repository to do so! Let us use this repository to play around changing this README and changing code together, just to learn the mechanics of git and git repositories. We will have other repositories for our projects.

### Some basic git/bitbucket commands ###

Normally, to *get* a repository's copy in your machine, you would go to the online repostory and copy the piece of terminal code in *clone*. For this repository, it would be:

    git clone git@bitbucket.org:modelab/lc-datasets.git

Then, you go in your machine, in terminal, wherever you want the repository to be, paste that, and do ENTER.

Now you have a *clone*. You can do changes. Try it out! Then, when you want to *upload* your code, synchronize your local machine with the online repo so as to effectively upload what you have done, yo have to:

* First, synchronize again in case you don't have the latests version:

    git pull 

If there is some conflict between what you have changed and what's online, you will have to solve it in your computer before proceeding.

* Then, take a *snapshot* of what you want to upload, generally all the repo:


    git add .


* Then, create a *commit*, some sort of nice package of changes, that will be put online, so that afterwards, when we see the evolution of some code, we can track its advancements in packages or *commits*. You have to give it some kind of self-explanatory title, like:


    git commit -m "Changing in README explaining git and bitbucket"

* Finally, when you have created your package of changes, or *commit*, you uploading, synchronizing what's online with your local version:

    git push

If all when ok, your local repository (what inside the folder *lc-playground*, in this case), is the same with what is in the online repo, so that others can use or review or sanction you code. This is how modern teams develop code in teams. It is essential, a prerequisite, to all the work of a DS o DE. 

These simple instructions are the very basic, minimalistic, procedures. Things will get more complicated, and there will be a lot of messes to fix in keeping our repository clean and well ordered in a nice sequence of packages of changes, *commits*. This repository is a playground for us to gain experience doing exactly that.

If you explore the online repository, you will quickly discover where commits are, and how that allows for tracking of a project's advancement.



### My first README section ###

What do I want to learn? How? What have I tried? Why am I not learning any more?


